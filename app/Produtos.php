<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model
{
    //define colunas "$fillable"
    protected $fillable = [
        'id', 'nome','descricao', 'preco'
    ];
    //Define Tabela $table
    protected $table = 'Produtos';
}
//Comentário pra dar o Commit
