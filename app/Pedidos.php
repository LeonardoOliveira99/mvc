<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    //define colunas "$fillable"
    protected $fillable = [
        'id', 'nomeCliente','quantidadeProdutos', 'idProdutos', 'valorTotal'
    ];
    //Define Tabela $table
    protected $table = 'Category';
}
