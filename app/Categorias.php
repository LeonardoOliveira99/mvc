<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
     //define colunas "$fillable"
     protected $fillable = [
        'id', 'nome','descricao'
    ];
    //Define Tabela $table
    protected $table = 'Category';
}
