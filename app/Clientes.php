<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
     //define colunas "$fillable"
     protected $fillable = [
        'id', 'nome','telefone', 'idade'
    ];
    //Define Tabela $table
    protected $table = 'Clientes';
}
