<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrinho extends Model
{
    //define colunas "$fillable"
    protected $fillable = [
        'id', 'Produtos','Total'
    ];
    //Define Tabela $table
    protected $table = 'Category';
}
